class TermsController < ApplicationController
  before_filter :find_product
  before_filter :find_term, only: [:update, :edit, :destroy]

  def index
  end

  def new
    @term = @product.terms.build
    @term.build_schedule
    @term.schedule.recurring_items.build
  end

  def create
    @term = @product.terms.new(term_params)
    if @term.save
      redirect_to edit_product_term_path(@product, @term), notice: 'Term is created.'
    else
      render 'new'
    end
  end

  def edit
    @term.build_schedule if @term.schedule.blank?
    @term.schedule.recurring_items.build if @term.schedule.recurring_items.blank?
  end

  def update
    if @term.update_attributes(term_params)
      redirect_to(edit_product_term_path, :notice => 'Term updated')
    else
      render :action => "edit"
    end
  end

  def destroy
    if @term.present?
      @term.destroy
      render :json => {:item => "Term destroyed"}
    else
      render_json_error("Term was not found")
    end
  end

  private

  def find_product
    @product = Product.find(params[:product_id])
  end

  def find_term
    @term = @product.terms.where(id: params[:id]).first
  end

  def term_params
    params.require(:term).permit( :start_date, :finish_date, :name,
                                    schedule_attributes: [:id, :schedulable_id, :schedulable_type,
                                                          oneoff_items_attributes: [:id, :schedule_id, :start, :finish, :_destroy],
                                                          recurring_items_attributes: [:id, :schedule_id, :schedule_yaml, :_destroy]
                                    ]
    )
  end

end