require 'chronic'

class BlackoutsController < ApplicationController
  before_filter  :find_products

  def new
    @blackout = current_account.blackouts.build

    if params[:all_day].present?
      @blackout.all_day = 1 if params[:all_day] == "true"
    end

    if params[:start].present?
      start = DateTime.parse(params[:start])
    else
      start = DateTime.now
    end

    finish = start + 1.hour
    if params[:finish].present?
      finish = DateTime.parse(params[:finish])
    end

    @blackout.start = start
    @blackout.finish = finish
  end

  def create
    @blackout = Blackout.new(blackout_params.except('id'))
    @blackout.shop = @account

    respond_to do |format|
      if @blackout.save
        format.html { redirect_to(events_url) }
        format.xml  { render :xml => @blackout, :status => :created, :location => @blackout }
        format.js
      else
        @variants = @blackout.product.present? ? @blackout.product.variants : []
        format.html { render :new, notice: "All Dates must be filled out." }
        format.xml  { render :xml => @blackout.errors, :status => :unprocessable_entity }
        format.js
      end
    end
  end

  def edit
    @blackout = @account.blackouts.where(id: params[:id]).first
    @blackout = @account.events.where(id: params[:id], type: "OldBlackout").first if @blackout.blank?
    if @blackout.present?
      @variants = @blackout.product.present? ? @blackout.product.variants : []
    else
      redirect_to events_url, alert: "Blackout not found"
    end
  end

  def update
    b = blackout_params

    @blackout = @account.blackouts.where(:id => params[:id]).first
    @blackout = @account.events.where(id: params[:id], type: "OldBlackout").first if @blackout.blank?
    if @blackout.present?


      if params[:delete]
        @blackout.delete
        redirect_to(events_url)
      else
        if @blackout.update_attributes(b)
          redirect_to(events_url)
        else
          @variants =  @blackout.product.present? ? @blackout.product.variants : []
          render :edit, notice: "The Start and Finish Dates must be Present"
        end
      end
    else
      redirect_to events_url, :notice => "The Blackout does not exist"
    end
  end

private
  def find_products
    @products = @account.products.order("product_title")
    @variants = []
  end


  def blackout_params
    params.require(:blackout).permit(
      :id,
      :product_id,
      :variant_id,
      :all_day,
      :start,
      :finish
    )
  end
end
