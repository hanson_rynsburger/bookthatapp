#Used to show calendar on /events page and to also unschedule
class EventsController < ApplicationController
  before_filter :ensure_not_blacklisted

  def index
    respond_to do |format|
      format.html do
        @latest_bookings = current_account.bookings.where('created_at < ?', (Time.now + 1.days).to_date).order('created_at desc').limit(5)
        @next_bookings = current_account.bookings.where('start >= ?', Time.now.to_date).order('start asc').limit(5)
        @products = current_account.products.order("product_title")
        @product_count = @products.count
      end

      format.json do
        start = params[:start].present? ? DateTime.parse(params[:start]) : DateTime.now
        finish = params[:end].present? ? DateTime.parse(params[:end]) : DateTime.now + 1.month
        product_id = params[:product]
        find_bookings = params[:booking].nil? || params[:booking] == 'true' || (params[:booking] == 'false' && params[:blackout] == 'false')
        find_blackouts = params[:blackout].nil? || params[:blackout] == 'true' || (params[:booking] == 'false' && params[:blackout] == 'false')
        find_available = true

            bookings = []
        if find_bookings
          bookings = booking_items(start, finish, product_id)
        end

        blackouts = []
        if find_blackouts
          blackouts = blackouts(start, finish, product_id)
        end

        available = []
        if find_available
          available = available(start, finish, product_id)
        end

        result = []
        (bookings + blackouts + available).uniq.each do |event|
          result << event.to_calendar_hash
        end

        @results = result

        render :json => {:events => @results}, :callback => params[:callback]
      end
    end
  end

  def booking_items(start, finish, product_id)
    items = current_account.booking_items.joins(:booking).where('(? <= booking_items.finish) AND (booking_items.start <= ?)', start, finish)
    items = items.where(:booking_items => {:product_id => product_id}) unless product_id.blank?
    items
  end

  def blackouts(start, finish, product_id)
    # EventDate based
    edbo = current_account.blackouts.where('(? <= finish) AND (start <= ?)', start, finish)
    edbo = edbo.where(:product_id => [product_id, nil]) if product_id.present?

    # Event based
    ebo = Event.where(:shop_id => current_account.id, :type => "OldBlackout").where('(? <= finish) AND (start <= ?)', start, finish)
    ebo = ebo.where(:product_id => [product_id, nil]) if product_id.present?

    edbo.all + ebo.all
  end

  def available(start, finish, product_ids)
    # current_account.seasons
    []
  end
end

# class Available
#
# end
