class Api::V2::ReservationsController < Api::V2::BaseController
  #before_action :authenticate

  def get
    params.require(:uuid)
    reservation = Reservation.find_by(uuid: params[:uuid])
    render json: reservation.to_json(:include => :reservation_items)
  end

  def create
    params = create_reservation_params
    reservation = current_shop.reservations.build(params)
    if reservation.save
      render json: reservation
    else
      render json: reservation.errors, status: :unprocessable_entity
    end
  end

  #this generates dummy data for reservation
  def save 
    p create_reservation_params[:uuid]
    p create_reservation_params[:expires]
    p create_reservation_params[:reservation_items]

    render json: { items: [{ variant_id: "1", qty: 2, start: '2016-06-06', finish: '2016-07-07'}, { variant_id: "1", qty: 2, start: '2016-06-06', finish: '2016-07-07'}], status: 1 } # 1 Success, 0 Fail
  end

  private
  def create_reservation_params
    params.require(:reservation).permit(:uuid, :expires, reservation_items: [:variant_id, :quantity, :start, :finish])
  end
end