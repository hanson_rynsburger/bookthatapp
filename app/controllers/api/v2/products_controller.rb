class Api::V2::ProductsController < Api::V2::BaseController
  before_action :authenticate

  def index 
  	render json: current_shop.products.to_json({include: :variants})
  end

  def get
  	params.require(:id)
  	product = current_shop.products.find_by(id: params[:id])
  	render json: product.to_json(include: :variants)
  end

  def getCapacity
  	params.require(:id)
  	variant = Variant.find params[:id]
    render json: { data: variant.capacity }
  end

end