class Api::V2::BaseController < ActionController::Base
  include Knock::Authenticable
  protect_from_forgery with: :null_session
  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  before_filter :destroy_session

protected
  def current_shop
    # TODO
    #current_user.shop
    current_user
  end

  def destroy_session
    request.session_options[:skip] = true
  end

  def not_found
    return api_error(status: 404, errors: 'Not found')
  end

  def api_error(status: 500, errors: [])
    unless Rails.env.production?
      puts errors.full_messages if errors.respond_to? :full_messages
    end
    head status: status and return if errors.empty?
    render json: jsonapi_format(errors).to_json, status: status
  end
end