class ProductImportsController < ApplicationController
  before_filter :setup_references

  def new
    @import = ProductImport.new
  end

  def create
    params = product_import_params
    import = ProductImport.new(params)
    import.shop = current_account
    import.attachment = params[:file]

    respond_to do |format|
      if import.save
        import.start
        format.html { redirect_to(product_imports_path) }
      else
        flash.now[:notice] = 'There was a problem saving the import'
        render action: 'new'
      end
    end
  end

  def index
    @imports = current_account.product_imports.order("id DESC")
  end

  def destroy
    flash[:notice] = 'Product import deleted.'
    ProductImport.find_by_id(params[:id]).destroy
    redirect_to action: :index
  end

  def setup_references
    @types = ProductProfiles.labels
  end

  def product_import_params
    params.require(:product_import).permit(
        :file,
        :profile,
        :mindate,
        :lead_time,
        :lag_time,
        :range_basis,
        :range_min,
        :range_max)
  end
end
