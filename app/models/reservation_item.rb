class ReservationItem < ActiveRecord::Base
  belongs_to :reservation, inverse_of: :reservation_items
end
