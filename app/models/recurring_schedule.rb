class RecurringSchedule < ScheduleItem
  def occurrences(start, finish)
    ice_schedule = icecube_schedule
    return [] if ice_schedule.blank? || ice_schedule.rrules.empty? # not a recurring schedule

    duration = ice_schedule.duration
    occurrences = ice_schedule.occurrences_between(start, finish)

    result = []
    occurrences.each do |occurrence| # next_occurrences
      if occurrence >= start
        result << {
            start: occurrence.start_time,
            finish: occurrence.end_time,
            duration: duration
        }
      end
    end

    result
  end

  def icecube_schedule
    unless schedule_yaml.blank?
      begin
        IceCube::Schedule.from_yaml(schedule_yaml)
      rescue Exception => e
        logger.error "Error loading schedule yaml: #{self.schedule_yaml}"
        logger.error e.message + "\n " + e.backtrace.join("\n ")
        IceCube::Schedule.new(Date.today.to_time.utc)
      end
    end
  end
end
