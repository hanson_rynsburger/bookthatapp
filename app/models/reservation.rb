class Reservation < ActiveRecord::Base
  belongs_to :shop, inverse_of: :reservations
  has_many :reservation_items, dependent: destroy
  accepts_nested_attributes_for :reservation_items, :allow_destroy => true
end
