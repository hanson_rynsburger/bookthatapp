class ScheduleItemPresenter < Presenter
  include PresenterHelper

  def calendar_json(start, finish, title)
    occurrences = @object.occurrences(start, finish)
    events = []
    occurrences.each_with_index   do |occurrence, index|
      event = CalendarItemPresenter.new.item_json(
          index,
          format_time(occurrence[:start]),
          nil,
          title.to_s
      )
      events.push(event)
    end
    events
  end

end
