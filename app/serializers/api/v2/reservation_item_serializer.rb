class Api::V1::ReservationItemSerializer < Api::V1::BaseSerializer
  attributes :variant_id, :quantity, :available, :capacity_code, :capacity_message
end