class Api::V1::ReservationSerializer < Api::V2::BaseSerializer
  attributes :uuid, :created, :updated, :expires
  has_many :reservation_items
end