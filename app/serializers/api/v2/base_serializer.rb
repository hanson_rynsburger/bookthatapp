class Api::V2::BaseSerializer < ActiveModel::Serializer
  def created
    object.created_at.in_time_zone.iso8601 if object.created_at
  end

  def updated
    object.updated_at.in_time_zone.iso8601 if object.created_at
  end
end