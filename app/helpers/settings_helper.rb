module SettingsHelper

  def reminder_templates_for_select
    @account.templates.reminders.map{ |template| [ "#{template.name} (#{TemplateChannel.label(template.channel)})" , template.id] }
  end

  def reminder_triggers_for_select
    TriggerType.labels
  end

end