module DocoHelper
  def to_time(time)
    time_ago_in_words(Time.parse(time))
  end

  def to_time(time)
    Time.parse(time)
  end
end