module ReportsHelper
  def display_product_search_options
    @product_id = params[:products] || ''

    products = @account.products.order('product_title')

    profile = params[:profile]
    products = products.where("profile = '#{profile}'") if profile.present?

    opts = ['<option value="">Select...</option>']
    products.each do |product|
      opts << "<option value='#{product.id}' #{"selected='selected'" if product.id.to_s == @product_id} data-external-id=#{product.external_id}>#{product.product_title}</option>"
    end
    opts.join(' ').html_safe
  end

  def display_product_variants
    opts = []
    Variant.where(:product_id => @product_id).each do |variant|
      opts << "<option value='#{variant.id}' data-external-id='#{variant.external_id}' #{"selected='selected'" if variant.id.to_s == @the_data.variant_id}>#{variant.title}</option>"
    end
    opts.join(' ').html_safe
  end

  def display_resources
    @resource_id = params[:resources] || ''
    opts = ['<option value="">Select Resource...</option>']
    @available_resources.each do |resource|
      opts << "<option value='#{resource.id}' #{"selected='selected'" if resource.id.to_s == @resource_id}>#{resource.name}</option>"
    end
    opts.join(' ').html_safe
  end

  def all_day_variants_only?(booking)
    booking.booking_items.all?{|bi| bi.variant.all_day != 0}
  end
end
