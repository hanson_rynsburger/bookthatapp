class DropReservationsTable < ActiveRecord::Migration
  def up
    drop_table :reservation_items
    drop_table :reservations
    remove_column :shops, :allow_waitlist
  end

  def down
  end
end
