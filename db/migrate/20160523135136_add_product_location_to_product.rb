class AddProductLocationToProduct < ActiveRecord::Migration
  def change
    add_column :products, :product_location, :string
  end
end
