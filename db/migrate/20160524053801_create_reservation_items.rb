class CreateReservationItems < ActiveRecord::Migration
  def change
    create_table :reservation_items do |t|
      t.belongs_to :reservation, index: true, foreign_key: true
      t.integer :external_variant_id, :limit => 8, null: false
      t.integer :quantity, null: false
      t.datetime :start, null: false
      t.datetime :finish, null: false
    end
  end
end
