class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.belongs_to :shop, index: true, foreign_key: true
      t.string :uuid
      t.datetime :expires_at
    end
  end
end
