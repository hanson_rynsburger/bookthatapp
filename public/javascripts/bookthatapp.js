'use strict';

/*!
 * BookThatApp JavaScript SDK
 * Version: 1.0.0
 * http://www.bookthatapp.com
 *
 * Copyright 2015 Shopify Concierge.
 */

var axios = require('axios');

function BookThatApp() {
    /**
     * Default config
     * @type {Object}
     */
    var config = {
        apiBaseUrl: 'https://api.bookthatapp.com/',
        apiVersion: 'v1'
    };

    /**
     * Root Object that holds methods to expose for API consumption
     * @type {Object}
     */
    var BTA = {};

    BTA.request = function(args) {
        args.url = config.apiBaseUrl + config.apiVersion + args.url;
        return axios(args);
    };

    /**
     * Overwrite default config with supplied settings
     * @type {Function}
     * @return {Object}
     */
    BTA.configure = function(custom) {
        for (var attr in custom) { config[attr] = custom[attr]; }
        return config;
    };

    /**
     * Returns the current config
     * @type {Function}
     * @return {Object}
     */
    BTA.getConfig = function() {
        return config;
    };

    /**
     * Get a user's anonymized availability (other user's on Timekit can be queryied by supplying their email)
     * @type {Function}
     * @return {Promise}
     */
    BTA.getAvailability = function(data) {
        return BTA.makeRequest({
            url: '/availability',
            method: 'get',
            params: data
        });
    };
}

module.exports = new BookThatApp();