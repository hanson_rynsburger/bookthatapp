'use strict';

var webpack = require("webpack");

module.exports = {
    entry: "./src/sdk.js",
    devtool: "source-map",
    output: {
        path: "./dist",
        filename: "bta-sdk.js",
        libraryTarget: "umd",
        library: "BookThatApp"
    },
    plugins: [
        new webpack.IgnorePlugin(/vertx/)
    ]
};
