'use strict';

var axios = require('axios');
var base64 = require('base-64');
var humps = require('humps');

function BookThatApp() {

	var accessToken;

	var includes = [];
	var headers = {};

	/**
	 * Default config
	 * @type {Object}
	 */
	var config = {
		app: 'BookThatApp',
		apiBaseUrl: 'http://api.bookthatapp.dev:3002/api/', /* for development */
		apiVersion: 'v2'
	};

	/* var encodeAuthHeader = function(email, token) {
		return base64.encode(email + ':' + token);
	}; */

	var buildUrl = function(endpoint) {
		return config.apiBaseUrl + config.apiVersion + endpoint;
	};

	/**
	 * Root Object that holds methods to expose for API consumption
	 * @type {Object}
	 */
	var btaw = {};


	/**
	 * Prepare and make HTTP request to API
	 * @type {Object}
	 * @return {Promise}
	 */
	btaw.makeRequest = function(args) {

		// construct URL with base, version and endpoint
	
		// add http headers if applicable
		args.headers = args.headers || headers || {};

		if (config.timezone) { args.headers['BookThatApp-Timezone'] = config.timezone; }

		// add auth headers if not being overwritten by request
		if (!args.headers['Authorization'] && accessToken ) {
			args.url = buildUrl(args.url);
			args.headers['Authorization'] = 'Bearer ' + accessToken;
		}
		else {
			args.url = config.apiBaseUrl + args.url;;
		}

		// reset headers
		if (Object.keys(headers).length > 0) {
			headers = {};
		}

		// add dynamic includes if applicable
		if (includes && includes.length > 0) {
			if (args.params === undefined) { args.params = {}; }
			args.params.include = includes.join();
			includes = [];
		}

		// decamelize keys in data objects
		if (args.data) { args.data = humps.decamelizeKeys(args.data); }

		// register response interceptor for data manipulation
		var interceptor = axios.interceptors.response.use(function (response) {
			if (response.data && response.data.data) {
				response.data = response.data.data;
				response.data = humps.camelizeKeys(response.data);
			}
			return response;
		}, function (error) {
			return Promise.reject(error);
		});

		// execute request!
		var request = axios(args);

		// deregister response interceptor
		axios.interceptors.response.eject(interceptor);

		return request;
	};

	btaw.configure = function(custom) {
		for (var attr in custom) { config[attr] = custom[attr]; }
		return config;
	};

	btaw.getConfig = function() {
		return config;
	};
 
	btaw.include = function() {
		includes = Array.prototype.slice.call(arguments);
		return this;
	};

	btaw.headers = function(data) {
		headers = data;
		return this;
	};

	btaw.init = function(data) {
		var r = btaw.makeRequest({
			url: 'auth_token',
			method: 'post',
			data: { auth: data },
		});

		r.then(function(response) {
			accessToken = response.data.jwt;
			// console.log( accessToken )
		}).catch(function(){
			accessToken = '';
		});

		return r;
	};

	/**
	 * Get list of apps
	 * @type {Function}
	 * @return {Promise}
	 */
	btaw.getProducts = function() {
		return btaw.makeRequest({
			url: '/products',
			method: 'get'
		});
	};

	btaw.getProduct = function( productId ) {
		return btaw.makeRequest({
			url: '/product/id/' + productId,
			method: 'get'
		});
	};

	btaw.getVariantCapacity = function( variantId ) {
		return btaw.makeRequest({
			url: '/product/variant/' + variantId + '/capacity',
			method: 'get'
		});
	};

	btaw.getAvailability = function( from, to, ids ) {
		return btaw.makeRequest({
			url: '/availability/ids/' + ids.join(',') + '/from/' + from + '/to/' + to,
			method: 'get'
		});
	}

	btaw.reserve = function( token, expiry_time, variants ) {
		return btaw.makeRequest({
			url: '/reserve/',
			method: 'post',
			data: { reservation: { token: token, expiry_time: expiry_time, variants: variants } },
		});
	}

	btaw.reservation = function ( data ) {
		return {
			CONSTANTS: {
				SUCCESS: 1,
				FAIL: 0
			},

			// _data: data,
			items: data.items,
			status: data.status,

			removeVariant: function( variantId ) {

			},

			updateVariantQuantity: function( variantId ) {

			},

			getSecondsUntilExpiry: function( ) {

			}
		};
	};

	return btaw;
}

module.exports = new BookThatApp();
