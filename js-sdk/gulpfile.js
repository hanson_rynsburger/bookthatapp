var gulp = require('gulp');
var connect = require('gulp-connect');

gulp.task( 'serve', function() {
	connect.server( {
		root: '',
		port: 3003,
		host: 'localhost',
		fallback: 'examples/test.html',
		livereload: true
	})
});

gulp.task( 'default', ['serve'] );