require 'test_helper'

class Api::V2::ProductsControllerTest < ActionController::TestCase
  def authenticate
    token = Knock::AuthToken.new(payload: {sub: users(:one).id}).token
    request.env['HTTP_AUTHORIZATION'] = "Bearer #{token}"
  end

  setup do
    authenticate
  end

  test 'get /api/v2/products works' do
    get :index, :format => :json
    assert_response :success
  end
end
