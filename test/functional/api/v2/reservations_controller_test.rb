require 'test_helper'

class Api::V2::ReservationsControllerTest < ActionController::TestCase
  def authenticate
    token = Knock::AuthToken.new(payload: {sub: users(:one).id}).token
    request.env['HTTP_AUTHORIZATION'] = "Bearer #{token}"
  end

  setup do
    authenticate
    @request.headers['Accept'] = 'application/json'
  end

  test 'post /api/v2/reservations works' do
    assert_difference 'Reservation.count' do
      post(:create, {reservation: {uuid: 'cart_token_123', reservation_items_attributes:[{variant_id:1, quantity:1, start:DateTime.now}]}})
    end

    assert_response :success

    json_response = JSON.parse(@response.body)
    assert_equal json_response['uuid'], 'cart_token_123'
  end
end
